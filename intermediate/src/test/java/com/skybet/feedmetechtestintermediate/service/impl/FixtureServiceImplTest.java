package com.skybet.feedmetechtestintermediate.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestintermediate.assembler.FixtureAssembler;
import com.skybet.feedmetechtestintermediate.assembler.OpportunityAssembler;
import com.skybet.feedmetechtestintermediate.assembler.WinningResultAssembler;
import com.skybet.feedmetechtestintermediate.domain.Fixture;
import com.skybet.feedmetechtestintermediate.domain.Opportunity;
import com.skybet.feedmetechtestintermediate.domain.WinningResult;
import com.skybet.feedmetechtestintermediate.model.datatype.Body;
import com.skybet.feedmetechtestintermediate.model.datatype.DataType;
import com.skybet.feedmetechtestintermediate.model.datatype.Header;
import com.skybet.feedmetechtestintermediate.model.datatype.event.Event;
import com.skybet.feedmetechtestintermediate.model.datatype.event.EventBody;
import com.skybet.feedmetechtestintermediate.model.datatype.market.Market;
import com.skybet.feedmetechtestintermediate.model.datatype.market.MarketBody;
import com.skybet.feedmetechtestintermediate.model.datatype.outcome.Outcome;
import com.skybet.feedmetechtestintermediate.model.datatype.outcome.OutcomeBody;
import com.skybet.feedmetechtestintermediate.model.exception.FixtureNotFoundException;
import com.skybet.feedmetechtestintermediate.model.exception.OpportunityNotFoundException;
import com.skybet.feedmetechtestintermediate.repository.FixtureRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FixtureServiceImplTest {

    @Mock private FixtureAssembler fixtureAssembler;

    @Mock private OpportunityAssembler opportunityAssembler;

    @Mock private WinningResultAssembler winningResultAssembler;

    @Mock private FixtureRepository fixtureRepository;

    @InjectMocks private FixtureServiceImpl fixtureServiceImpl;

    @Test
    public void handleDataTypeShouldThrowIllegalArgumentExceptionDueToUnknownDataType() {
        // Given
        class UnknownDataType extends DataType {

            public UnknownDataType(final Header header, final Body body) {
                super(header, body);
            }
        }
        final UnknownDataType unknownDataType = mock(UnknownDataType.class);

        // When
        final Throwable throwable = catchThrowable(() -> fixtureServiceImpl.handleDataType(unknownDataType));

        // Then
        assertThat(throwable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("behavior not implemented for %s", unknownDataType.getClass().getSimpleName());
    }

    @Test
    public void handleCreateEvent() {
        // Given
        final String operation = "create";
        final Header header = mock(Header.class);
        final Event event = mock(Event.class);
        final Fixture fixture = mock(Fixture.class);
        when(header.getOperation()).thenReturn(operation);
        when(event.getHeader()).thenReturn(header);
        when(fixtureAssembler.mapFrom(event)).thenReturn(fixture);

        // When
        fixtureServiceImpl.handleDataType(event);

        // Then
        verify(fixtureAssembler).mapFrom(event);
        verify(fixtureRepository).save(fixture);
    }

    @Test
    public void handleUpdateEvent() {
        // Given
        final String operation = "update";
        final String id = "id";
        final Header header = mock(Header.class);
        final EventBody eventBody = mock(EventBody.class);
        final Event event = mock(Event.class);
        final Fixture fixture = mock(Fixture.class);
        when(header.getOperation()).thenReturn(operation);
        when(eventBody.getEventId()).thenReturn(id);
        when(event.getHeader()).thenReturn(header);
        when(event.getBody()).thenReturn(eventBody);
        when(fixtureRepository.findOne(id)).thenReturn(fixture);

        // When
        fixtureServiceImpl.handleDataType(event);

        // Then
        verify(fixtureRepository).findOne(id);
        verify(fixtureAssembler).update(fixture, event);
        verify(fixtureRepository).save(fixture);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void handleUpdateEventShouldThrowFixtureNotFoundException() {
        // Given
        final String operation = "update";
        final String id = "id";
        final Header header = mock(Header.class);
        final EventBody eventBody = mock(EventBody.class);
        final Event event = mock(Event.class);
        when(header.getOperation()).thenReturn(operation);
        when(eventBody.getEventId()).thenReturn(id);
        when(event.getHeader()).thenReturn(header);
        when(event.getBody()).thenReturn(eventBody);
        when(fixtureRepository.findOne(id)).thenThrow(new FixtureNotFoundException(id));

        // When
        final Throwable throwable = catchThrowable(() -> fixtureServiceImpl.handleDataType(event));

        // Then
        verify(fixtureRepository).findOne(id);
        verify(fixtureAssembler, never()).update(any(Fixture.class), eq(event));
        verify(fixtureRepository, never()).save(any(Fixture.class));
        assertThat(throwable)
                .isInstanceOf(FixtureNotFoundException.class)
                .hasMessage("fixture with id %s not found", id);
    }

    @Test
    public void handleMarket() {
        // Given
        final String id = "id";
        final MarketBody marketBody = mock(MarketBody.class);
        final Market market = mock(Market.class);
        final Opportunity opportunity = mock(Opportunity.class);
        final Fixture fixture = mock(Fixture.class);
        when(marketBody.getEventId()).thenReturn(id);
        when(market.getBody()).thenReturn(marketBody);
        when(opportunityAssembler.mapFrom(market)).thenReturn(opportunity);
        when(fixtureRepository.findOne(id)).thenReturn(fixture);

        // When
        fixtureServiceImpl.handleDataType(market);

        // Then
        verify(opportunityAssembler).mapFrom(market);
        verify(fixtureRepository).findOne(id);
        verify(fixture).addOpportunity(opportunity);
        verify(fixtureRepository).save(fixture);
    }

    @Test
    public void handleMarketShouldThrowFixtureNotFoundException() {
        // Given
        final String id = "id";
        final MarketBody marketBody = mock(MarketBody.class);
        final Market market = mock(Market.class);
        when(marketBody.getEventId()).thenReturn(id);
        when(market.getBody()).thenReturn(marketBody);
        when(fixtureRepository.findOne(id)).thenThrow(new FixtureNotFoundException(id));

        // When
        final Throwable throwable = catchThrowable(() -> fixtureServiceImpl.handleDataType(market));

        // Then
        verify(opportunityAssembler).mapFrom(market);
        verify(fixtureRepository).findOne(id);
        verify(fixtureRepository, never()).save(any(Fixture.class));
        assertThat(throwable)
                .isInstanceOf(FixtureNotFoundException.class)
                .hasMessage("fixture with id %s not found", id);
    }

    @Test
    public void handleOutcome() {
        // Given
        final String id = "id";
        final OutcomeBody outcomeBody = mock(OutcomeBody.class);
        final Outcome outcome = mock(Outcome.class);
        final WinningResult winningResult = mock(WinningResult.class);
        final Opportunity opportunity = mock(Opportunity.class);
        final Fixture fixture = mock(Fixture.class);
        when(outcomeBody.getMarketId()).thenReturn(id);
        when(outcome.getBody()).thenReturn(outcomeBody);
        when(fixture.getOpportunity(id)).thenReturn(opportunity);
        when(winningResultAssembler.mapFrom(outcome)).thenReturn(winningResult);
        when(fixtureRepository.findContainingOpportunityWithId(id)).thenReturn(fixture);

        // When
        fixtureServiceImpl.handleDataType(outcome);

        // Then
        verify(winningResultAssembler).mapFrom(outcome);
        verify(fixtureRepository).findContainingOpportunityWithId(id);
        verify(opportunity).addWinningResult(winningResult);
        verify(fixture).addOpportunity(opportunity);
        verify(fixtureRepository).save(fixture);
    }

    @Test
    public void handleOutcomeShouldThrowOpportunityNotFoundException() {
        // Given
        final String id = "id";
        final OutcomeBody outcomeBody = mock(OutcomeBody.class);
        final Outcome outcome = mock(Outcome.class);
        when(outcomeBody.getMarketId()).thenReturn(id);
        when(outcome.getBody()).thenReturn(outcomeBody);
        when(fixtureRepository.findContainingOpportunityWithId(id)).thenThrow(new OpportunityNotFoundException(id));

        // When
        final Throwable throwable = catchThrowable(() -> fixtureServiceImpl.handleDataType(outcome));

        // Then
        verify(winningResultAssembler).mapFrom(outcome);
        verify(fixtureRepository).findContainingOpportunityWithId(id);
        verify(fixtureRepository, never()).save(any(Fixture.class));
        assertThat(throwable)
                .isInstanceOf(OpportunityNotFoundException.class)
                .hasMessage("opportunity with id %s not found", id);
    }
}
