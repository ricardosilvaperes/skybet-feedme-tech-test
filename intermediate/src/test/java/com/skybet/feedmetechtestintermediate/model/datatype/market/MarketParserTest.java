package com.skybet.feedmetechtestintermediate.model.datatype.market;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestintermediate.model.datatype.DataTypeParserTest;
import com.skybet.feedmetechtestintermediate.model.datatype.DataTypeParserUtils;
import com.skybet.feedmetechtestintermediate.model.datatype.Header;
import com.skybet.feedmetechtestintermediate.model.exception.InvalidPacketException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MarketParserTest extends DataTypeParserTest {

    @Mock private DataTypeParserUtils dataTypeParserUtils;

    @Mock private MarketBodyValidator marketBodyValidator;

    @InjectMocks private MarketParser marketParser;

    @Test
    public void parse() {
        // Given
        final int msgId = 1;
        final String strMsgId = String.valueOf(msgId);
        final String operation = "create";
        final String type = "market";
        final long timestamp = 1L;
        final String strTimestamp = String.valueOf(timestamp);
        final String eventId = "eventId";
        final String marketId = "marketId";
        final String name = "name";
        final boolean displayed = false;
        final String strDisplayed = "0";
        final boolean suspended = true;
        final String strSuspended = "1";
        final String[] headerValues = {strMsgId, operation, type, strTimestamp};
        final String bodyValue = "bodyValue";
        final String[] marketValues = {eventId, marketId, name, strDisplayed, strSuspended};
        when(marketBodyValidator.dataIsValid(bodyValue)).thenReturn(true);
        when(dataTypeParserUtils.splitBodyValue(bodyValue)).thenReturn(marketValues);
        when(dataTypeParserUtils.booleanValueOf(strDisplayed)).thenReturn(false);
        when(dataTypeParserUtils.booleanValueOf(strSuspended)).thenReturn(true);

        // When
        final Market actualMarket = marketParser.parse(headerValues, bodyValue);

        // Then
        assertThat(actualMarket).isNotNull();
        final Header actualMarketHeader = actualMarket.getHeader();
        assertHeader(actualMarketHeader, msgId, operation, type, timestamp);
        final MarketBody actualMarketBody = actualMarket.getBody();
        assertThat(actualMarketBody.getEventId()).isEqualTo(eventId);
        assertThat(actualMarketBody.getMarketId()).isEqualTo(marketId);
        assertThat(actualMarketBody.getName()).isEqualTo(name);
        assertThat(actualMarketBody.isDisplayed()).isEqualTo(displayed);
        assertThat(actualMarketBody.isSuspended()).isEqualTo(suspended);
    }

    @Test
    public void parseShouldThrowInvalidPacketException() {
        // Given
        final String[] headerValues = {};
        final String bodyValue = "bodyValue";
        when(marketBodyValidator.dataIsValid(bodyValue)).thenReturn(false);

        // When
        final Throwable throwable = catchThrowable(() -> marketParser.parse(headerValues, bodyValue));

        // Then
        assertThat(throwable)
                .isInstanceOf(InvalidPacketException.class)
                .hasMessage("invalid packet data detected: %s", bodyValue);
    }
}
