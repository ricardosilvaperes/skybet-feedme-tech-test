package com.skybet.feedmetechtestintermediate.model.datatype.outcome;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class OutcomeBodyValidatorTest {

    private final OutcomeBodyValidator outcomeBodyValidator = new OutcomeBodyValidator();

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidMarketId() {
        // Given
        final String data = "invalidMarket|00000000-0000-0000-0000-000000000000|name|1/1|0|1";

        // When
        final boolean result = outcomeBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidOutcomeId() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|invalidOutcomeId|name|1/1|0|1";

        // When
        final boolean result = outcomeBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidName() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|00000000-0000-0000-0000-000000000000||1/1|0|1";

        // When
        final boolean result = outcomeBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidPrice() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|00000000-0000-0000-0000-000000000000|name|invalidPrice|0|1";

        // When
        final boolean result = outcomeBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidDisplayedValue() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|00000000-0000-0000-0000-000000000000|name|1/1|invalidDisplayedValue|1";

        // When
        final boolean result = outcomeBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidSuspendedValue() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|00000000-0000-0000-0000-000000000000|name|1/1|0|invalidSuspendedValue";

        // When
        final boolean result = outcomeBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnTrue() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|00000000-0000-0000-0000-000000000000|name|1/1|0|1";

        // When
        final boolean result = outcomeBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isTrue();
    }
}
