package com.skybet.feedmetechtestintermediate.model.packet;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class PacketParserUtilsTest {

    private final PacketParserUtils packetParserUtils = new PacketParserUtils();

    @Test
    public void splitPacket() {
        // Given
        final String msgId = "msgId";
        final String operation = "operation";
        final String type = "type";
        final String timestamp = "timestamp";
        final String body = "body";
        final String packet = "|" + msgId + "|" + operation + "|" + type + "|" + timestamp + "|" + body + "|";

        // When
        final String[] actualPacketValues = packetParserUtils.splitPacket(packet);

        // Then
        assertThat(actualPacketValues).containsExactly(type, msgId, operation, type, timestamp, body);
    }
}
