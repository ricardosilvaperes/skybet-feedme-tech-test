package com.skybet.feedmetechtestintermediate.model.packet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestintermediate.model.exception.InvalidPacketException;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PacketParserTest {

    @Mock private PacketValidator packetValidator;

    @Mock private PacketParserUtils packetParserUtils;

    @InjectMocks private PacketParser packetParser;

    @Test
    public void parse() {
        // Given
        final String msgId = "msgId";
        final String operation = "operation";
        final String type = "type";
        final String timestamp = "timestamp";
        final String body = "body";
        final String[] headerValues = {msgId, operation, type, timestamp};
        final String[] typeAndHeadersValuesAndBodyValue = new String[6];
        typeAndHeadersValuesAndBodyValue[0] = type;
        typeAndHeadersValuesAndBodyValue[1] = msgId;
        typeAndHeadersValuesAndBodyValue[2] = operation;
        typeAndHeadersValuesAndBodyValue[3] = type;
        typeAndHeadersValuesAndBodyValue[4] = timestamp;
        typeAndHeadersValuesAndBodyValue[5] = body;
        final String data = "|" + msgId + "|" + operation + "|" + type + "|" + timestamp + "|" + body;
        when(packetValidator.dataIsValid(data)).thenReturn(true);
        when(packetParserUtils.splitPacket(data)).thenReturn(typeAndHeadersValuesAndBodyValue);

        // When
        final Packet actualPacket = packetParser.parse(data);

        // Then
        Assertions.assertThat(actualPacket).isNotNull();
        Assertions.assertThat(actualPacket.getTypeValue()).isEqualTo(type);
        Assertions.assertThat(actualPacket.getHeaderValues()).isNotNull();
        final String[] actualHeaderValues = actualPacket.getHeaderValues();
        Assertions.assertThat(actualHeaderValues).containsExactly(headerValues);
        Assertions.assertThat(actualPacket.getBodyValue()).isEqualTo(body);
    }

    @Test
    public void parseShouldThrowInvalidPacketExceptionDueToInvalidData() {
        // Given
        final String invalidData = "invalidData";
        when(packetValidator.dataIsValid(invalidData)).thenReturn(false);

        // When
        final Throwable throwable = catchThrowable(() -> packetParser.parse(invalidData));

        // Then
        assertThat(throwable)
                .isInstanceOf(InvalidPacketException.class)
                .hasMessage("invalid packet data detected: %s", invalidData);
    }
}
