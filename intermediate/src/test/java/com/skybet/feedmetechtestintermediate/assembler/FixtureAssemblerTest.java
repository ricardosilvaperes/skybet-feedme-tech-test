package com.skybet.feedmetechtestintermediate.assembler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestintermediate.domain.Fixture;
import com.skybet.feedmetechtestintermediate.model.datatype.Header;
import com.skybet.feedmetechtestintermediate.model.datatype.event.Event;
import com.skybet.feedmetechtestintermediate.model.datatype.event.EventBody;

import org.junit.Test;

public class FixtureAssemblerTest {

    private final FixtureAssembler fixtureAssembler = new FixtureAssembler();

    @Test
    public void mapFrom() {
        // Given
        final String id = "id";
        final String category = "category";
        final String subCategory = "subCategory";
        final String name = "name";
        final long startTime = 1L;
        final boolean displayed = true;
        final boolean suspended = false;
        final Header header = mock(Header.class);
        final EventBody eventBody = mock(EventBody.class);
        final Event event = new Event(header, eventBody);
        when(eventBody.getEventId()).thenReturn(id);
        when(eventBody.getCategory()).thenReturn(category);
        when(eventBody.getSubCategory()).thenReturn(subCategory);
        when(eventBody.getName()).thenReturn(name);
        when(eventBody.getStartTime()).thenReturn(startTime);
        when(eventBody.isDisplayed()).thenReturn(displayed);
        when(eventBody.isSuspended()).thenReturn(suspended);

        // When
        final Fixture actualFixture = fixtureAssembler.mapFrom(event);

        // Then
        assertThat(actualFixture).isNotNull();
        assertThat(actualFixture.getId()).isEqualTo(id);
        assertThat(actualFixture.getCategory()).isEqualTo(category);
        assertThat(actualFixture.getSubCategory()).isEqualTo(subCategory);
        assertThat(actualFixture.getName()).isEqualTo(name);
        assertThat(actualFixture.getStartTime()).isEqualTo(startTime);
        assertThat(actualFixture.isDisplayed()).isEqualTo(displayed);
        assertThat(actualFixture.isSuspended()).isEqualTo(suspended);
    }

    @Test
    public void update() {
        // Given
        final String id = "id";
        final String category = "category";
        final String subCategory = "subCategory";
        final String name = "name";
        final long startTime = 1L;
        final Header header = mock(Header.class);
        final EventBody eventBody = mock(EventBody.class);
        final Event event = new Event(header, eventBody);
        when(eventBody.getEventId()).thenReturn(id);
        when(eventBody.getCategory()).thenReturn(category);
        when(eventBody.getSubCategory()).thenReturn(subCategory);
        when(eventBody.getName()).thenReturn(name);
        when(eventBody.getStartTime()).thenReturn(startTime);
        when(eventBody.isDisplayed()).thenReturn(false);
        when(eventBody.isSuspended()).thenReturn(true);

        // When
        final Fixture fixture = fixtureAssembler.mapFrom(event);
        fixtureAssembler.update(fixture, event);

        // Then
        assertThat(fixture).isNotNull();
        assertThat(fixture.getId()).isEqualTo(id);
        assertThat(fixture.getCategory()).isEqualTo(category);
        assertThat(fixture.getSubCategory()).isEqualTo(subCategory);
        assertThat(fixture.getName()).isEqualTo(name);
        assertThat(fixture.getStartTime()).isEqualTo(startTime);
        assertThat(fixture.isDisplayed()).isFalse();
        assertThat(fixture.isSuspended()).isTrue();
    }
}
