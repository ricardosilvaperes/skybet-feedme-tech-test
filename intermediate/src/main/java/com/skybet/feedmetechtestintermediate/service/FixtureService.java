package com.skybet.feedmetechtestintermediate.service;

import com.skybet.feedmetechtestintermediate.model.datatype.DataType;

public interface FixtureService {

    void handleDataType(final DataType dataType);
}
