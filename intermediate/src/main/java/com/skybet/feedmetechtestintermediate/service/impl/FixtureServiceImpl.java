package com.skybet.feedmetechtestintermediate.service.impl;

import com.skybet.feedmetechtestintermediate.assembler.FixtureAssembler;
import com.skybet.feedmetechtestintermediate.assembler.OpportunityAssembler;
import com.skybet.feedmetechtestintermediate.assembler.WinningResultAssembler;
import com.skybet.feedmetechtestintermediate.domain.Fixture;
import com.skybet.feedmetechtestintermediate.domain.Opportunity;
import com.skybet.feedmetechtestintermediate.domain.WinningResult;
import com.skybet.feedmetechtestintermediate.model.datatype.DataType;
import com.skybet.feedmetechtestintermediate.model.datatype.event.Event;
import com.skybet.feedmetechtestintermediate.model.datatype.market.Market;
import com.skybet.feedmetechtestintermediate.model.datatype.outcome.Outcome;
import com.skybet.feedmetechtestintermediate.model.exception.FixtureNotFoundException;
import com.skybet.feedmetechtestintermediate.model.exception.OpportunityNotFoundException;
import com.skybet.feedmetechtestintermediate.repository.FixtureRepository;
import com.skybet.feedmetechtestintermediate.service.FixtureService;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FixtureServiceImpl implements FixtureService {

    private static final String UNKNOWN_DATA_TYPE_MESSAGE_TEMPLATE = "behavior not implemented for %s";

    private final FixtureAssembler fixtureAssembler;
    private final OpportunityAssembler opportunityAssembler;
    private final WinningResultAssembler winningResultAssembler;
    private final FixtureRepository fixtureRepository;

    public FixtureServiceImpl(final FixtureAssembler fixtureAssembler,
            final OpportunityAssembler opportunityAssembler,
            final WinningResultAssembler winningResultAssembler,
            final FixtureRepository fixtureRepository)
    {
        this.fixtureAssembler = fixtureAssembler;
        this.opportunityAssembler = opportunityAssembler;
        this.winningResultAssembler = winningResultAssembler;
        this.fixtureRepository = fixtureRepository;
    }

    @Override
    public void handleDataType(final DataType dataType) {
        if (dataType instanceof Event) {
            handleEvent((Event) dataType);
        } else if (dataType instanceof Market) {
            handleMarket((Market) dataType);
        } else if (dataType instanceof Outcome) {
            handleOutcome((Outcome) dataType);
        } else {
            throw new IllegalArgumentException(String.format(UNKNOWN_DATA_TYPE_MESSAGE_TEMPLATE, dataType.getClass().getSimpleName()));
        }
    }

    private void handleEvent(final Event event) {
        final String operation = event.getHeader().getOperation();
        Fixture fixture = null;
        if ("create".equals(operation)) {
            fixture = fixtureAssembler.mapFrom(event);
        } else if ("update".equals(operation)) {
            final String fixtureId = event.getBody().getEventId();
            fixture = Optional.ofNullable(fixtureRepository.findOne(fixtureId))
                    .orElseThrow(() -> new FixtureNotFoundException(fixtureId));
            fixtureAssembler.update(fixture, event);
        }
        fixtureRepository.save(fixture);
    }

    private void handleMarket(final Market market) {
        final String fixtureId = market.getBody().getEventId();
        final Opportunity opportunity = opportunityAssembler.mapFrom(market);
        final Fixture fixture = Optional.ofNullable(fixtureRepository.findOne(fixtureId))
                .orElseThrow(() -> new FixtureNotFoundException(fixtureId));
        fixture.addOpportunity(opportunity);
        fixtureRepository.save(fixture);
    }

    private void handleOutcome(final Outcome outcome) {
        final String opportunityId = outcome.getBody().getMarketId();
        final WinningResult winningResult = winningResultAssembler.mapFrom(outcome);
        final Fixture fixture = Optional.ofNullable(fixtureRepository.findContainingOpportunityWithId(opportunityId))
                .orElseThrow(() -> new OpportunityNotFoundException(opportunityId));
        final Opportunity opportunity = fixture.getOpportunity(opportunityId);
        opportunity.addWinningResult(winningResult);
        fixture.addOpportunity(opportunity);
        fixtureRepository.save(fixture);
    }
}
