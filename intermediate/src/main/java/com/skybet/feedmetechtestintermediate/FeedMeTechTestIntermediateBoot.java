package com.skybet.feedmetechtestintermediate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedMeTechTestIntermediateBoot {

    public static void main(String[] args) {
        SpringApplication.run(FeedMeTechTestIntermediateBoot.class, args);
    }
}
