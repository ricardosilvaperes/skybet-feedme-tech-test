package com.skybet.feedmetechtestintermediate.assembler;

import com.skybet.feedmetechtestintermediate.domain.WinningResult;
import com.skybet.feedmetechtestintermediate.model.datatype.outcome.Outcome;
import com.skybet.feedmetechtestintermediate.model.datatype.outcome.OutcomeBody;

import org.springframework.stereotype.Component;

@Component
public class WinningResultAssembler {

    public WinningResult mapFrom(final Outcome outcome) {
        final OutcomeBody outcomeBody = outcome.getBody();
        final String id = outcomeBody.getOutcomeId();
        final String name = outcomeBody.getName();
        final String price = outcomeBody.getPrice();
        final boolean displayed = outcomeBody.isDisplayed();
        final boolean suspended = outcomeBody.isSuspended();
        return new WinningResult(id, name, price, displayed, suspended);
    }
}
