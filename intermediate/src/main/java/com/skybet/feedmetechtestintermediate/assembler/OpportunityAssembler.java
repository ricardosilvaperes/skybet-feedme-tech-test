package com.skybet.feedmetechtestintermediate.assembler;

import com.skybet.feedmetechtestintermediate.domain.Opportunity;
import com.skybet.feedmetechtestintermediate.model.datatype.market.Market;
import com.skybet.feedmetechtestintermediate.model.datatype.market.MarketBody;

import org.springframework.stereotype.Component;

@Component
public class OpportunityAssembler {

    public Opportunity mapFrom(final Market market) {
        final MarketBody marketBody = market.getBody();
        final String id = marketBody.getMarketId();
        final String name = marketBody.getName();
        final boolean displayed = marketBody.isDisplayed();
        final boolean suspended = marketBody.isSuspended();
        return new Opportunity(id, name, displayed, suspended);
    }
}
