package com.skybet.feedmetechtestintermediate.repository;

import com.skybet.feedmetechtestintermediate.domain.Fixture;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FixtureRepository extends MongoRepository<Fixture, String> {

    @Query("{ 'opportunities.id' : ?0 }")
    Fixture findContainingOpportunityWithId(final String opportunityId);
}
