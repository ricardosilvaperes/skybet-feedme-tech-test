package com.skybet.feedmetechtestintermediate.model.datatype.outcome;

import com.skybet.feedmetechtestintermediate.model.datatype.DataTypeParser;
import com.skybet.feedmetechtestintermediate.model.datatype.DataTypeParserUtils;
import com.skybet.feedmetechtestintermediate.model.datatype.Header;
import com.skybet.feedmetechtestintermediate.model.exception.InvalidPacketException;

import org.springframework.stereotype.Component;

@Component("outcome")
public class OutcomeParser extends DataTypeParser {

    public OutcomeParser(final DataTypeParserUtils dataTypeParserUtils, final OutcomeBodyValidator outcomeValidator) {
        super(dataTypeParserUtils, outcomeValidator);
    }

    public Outcome parse(final String[] headerValues, final String bodyValue) {
        if (dataTypeBodyValidator.dataIsValid(bodyValue)) {
            final Header header = parse(headerValues);
            final String[] outcomeValues = dataTypeParserUtils.splitBodyValue(bodyValue);
            final String marketId = outcomeValues[0];
            final String outcomeId = outcomeValues[1];
            final String name = outcomeValues[2];
            final String price = outcomeValues[3];
            final boolean displayed = dataTypeParserUtils.booleanValueOf(outcomeValues[4]);
            final boolean suspended = dataTypeParserUtils.booleanValueOf(outcomeValues[5]);
            final OutcomeBody outcomeBody = new OutcomeBody(marketId, outcomeId, name, price, displayed, suspended);
            return new Outcome(header, outcomeBody);
        }
        throw new InvalidPacketException(bodyValue);
    }
}

