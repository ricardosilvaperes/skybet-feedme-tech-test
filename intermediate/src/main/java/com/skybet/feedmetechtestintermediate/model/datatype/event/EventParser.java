package com.skybet.feedmetechtestintermediate.model.datatype.event;

import com.skybet.feedmetechtestintermediate.model.datatype.DataTypeParser;
import com.skybet.feedmetechtestintermediate.model.datatype.DataTypeParserUtils;
import com.skybet.feedmetechtestintermediate.model.datatype.Header;
import com.skybet.feedmetechtestintermediate.model.exception.InvalidPacketException;

import org.springframework.stereotype.Component;

@Component("event")
public class EventParser extends DataTypeParser {

    public EventParser(final DataTypeParserUtils dataTypeParserUtils, final EventBodyValidator eventBodyValidator) {
        super(dataTypeParserUtils, eventBodyValidator);
    }

    public Event parse(final String[] headerValues, final String bodyValue) {
        if (dataTypeBodyValidator.dataIsValid(bodyValue)) {
            final Header header = parse(headerValues);
            final String[] eventValues = dataTypeParserUtils.splitBodyValue(bodyValue);
            final String eventId = eventValues[0];
            final String category = eventValues[1];
            final String subCategory = eventValues[2];
            final String name = eventValues[3];
            final long startTime = Long.parseLong(eventValues[4]);
            final boolean displayed = dataTypeParserUtils.booleanValueOf(eventValues[5]);
            final boolean suspended = dataTypeParserUtils.booleanValueOf(eventValues[6]);
            final EventBody eventBody = new EventBody(eventId, category, subCategory, name, startTime, displayed, suspended);
            return new Event(header, eventBody);
        }
        throw new InvalidPacketException(bodyValue);
    }
}

