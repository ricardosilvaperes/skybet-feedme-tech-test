package com.skybet.feedmetechtestintermediate.model.datatype.event;

import com.skybet.feedmetechtestintermediate.model.datatype.DataTypeBodyValidator;

import org.springframework.stereotype.Component;

@Component
public class EventBodyValidator extends DataTypeBodyValidator {

    private static final String EVENT_PATTERN_REGEX = UUID_REGEX + "\\|.+\\|.+\\|.+\\|\\d+\\|(0|1)\\|(0|1)";

    public EventBodyValidator() {
        super(EVENT_PATTERN_REGEX);
    }
}
