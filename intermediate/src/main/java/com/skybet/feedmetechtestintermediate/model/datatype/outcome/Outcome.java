package com.skybet.feedmetechtestintermediate.model.datatype.outcome;

import com.skybet.feedmetechtestintermediate.model.datatype.DataType;
import com.skybet.feedmetechtestintermediate.model.datatype.Header;

public class Outcome extends DataType<OutcomeBody> {

    public Outcome(final Header header, final OutcomeBody outcomeBody) {
        super(header, outcomeBody);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Outcome{");
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
