package com.skybet.feedmetechtestintermediate.model.packet;

import java.util.Arrays;

public final class Packet {

    private final String typeValue;
    private final String[] headerValues;
    private final String bodyValue;

    public Packet(final String typeValue, final String[] headerValues, final String bodyValue) {
        this.typeValue = typeValue;
        this.headerValues = headerValues;
        this.bodyValue = bodyValue;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public String[] getHeaderValues() {
        return headerValues;
    }

    public String getBodyValue() {
        return bodyValue;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Packet{");
        stringBuilder.append("typeValue='").append(typeValue).append('\'');
        stringBuilder.append(", headerValues=").append(Arrays.toString(headerValues));
        stringBuilder.append(", bodyValue='").append(bodyValue).append('\'');
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}

