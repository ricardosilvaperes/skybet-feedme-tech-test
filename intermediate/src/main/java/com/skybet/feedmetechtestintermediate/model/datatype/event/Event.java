package com.skybet.feedmetechtestintermediate.model.datatype.event;

import com.skybet.feedmetechtestintermediate.model.datatype.DataType;
import com.skybet.feedmetechtestintermediate.model.datatype.Header;

public class Event extends DataType<EventBody> {

    public Event(final Header header, final EventBody eventBody) {
        super(header, eventBody);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Event{");
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
