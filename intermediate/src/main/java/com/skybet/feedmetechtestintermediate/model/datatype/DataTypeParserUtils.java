package com.skybet.feedmetechtestintermediate.model.datatype;

import org.springframework.stereotype.Component;

@Component
public class DataTypeParserUtils {

    private static String removeEscapedPipes(final String str) {
        return str.replaceAll("\\\\\\|", "");
    }

    private static String[] splitBodyValues(final String bodyValue) {
        return bodyValue.split("\\|");
    }

    public boolean booleanValueOf(final String str) {
        return !str.equals("0");
    }

    public String[] splitBodyValue(final String bodyValue) {
        final String bodyValueWithoutEscapedPipes = removeEscapedPipes(bodyValue);
        return splitBodyValues(bodyValueWithoutEscapedPipes);
    }
}

