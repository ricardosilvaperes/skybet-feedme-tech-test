package com.skybet.feedmetechtestintermediate.model.datatype.market;

import com.skybet.feedmetechtestintermediate.model.datatype.Body;

public class MarketBody extends Body {

    private final String eventId;
    private final String marketId;
    private final String name;

    public MarketBody(final String eventId, final String marketId, final String name, final boolean displayed, final boolean suspended) {
        super(displayed, suspended);
        this.eventId = eventId;
        this.marketId = marketId;
        this.name = name;
    }

    public String getEventId() {
        return eventId;
    }

    public String getMarketId() {
        return marketId;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{eventId='").append(eventId).append('\'');
        stringBuilder.append(", marketId='").append(marketId).append('\'');
        stringBuilder.append(", name='").append(name).append('\'');
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
