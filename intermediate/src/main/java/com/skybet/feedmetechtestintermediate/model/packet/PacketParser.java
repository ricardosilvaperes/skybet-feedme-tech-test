package com.skybet.feedmetechtestintermediate.model.packet;

import com.skybet.feedmetechtestintermediate.model.exception.InvalidPacketException;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Optional;

@Component
public class PacketParser {

    private final PacketValidator packetValidator;
    private final PacketParserUtils packetParserUtils;

    public PacketParser(final PacketValidator packetValidator, final PacketParserUtils packetParserUtils) {
        this.packetValidator = packetValidator;
        this.packetParserUtils = packetParserUtils;
    }

    public Packet parse(final String data) {
        return Optional.ofNullable(data)
                .filter(packetValidator::dataIsValid)
                .map(this::buildPacket)
                .orElseThrow(() -> new InvalidPacketException(data));
    }

    private Packet buildPacket(final String data) {
        final String[] typeAndHeadersValuesAndBodyValue = packetParserUtils.splitPacket(data);
        final String typeValue = typeAndHeadersValuesAndBodyValue[0];
        final String[] headerValues = Arrays.copyOfRange(typeAndHeadersValuesAndBodyValue, 1, typeAndHeadersValuesAndBodyValue.length - 1);
        final String bodyValue = typeAndHeadersValuesAndBodyValue[typeAndHeadersValuesAndBodyValue.length - 1];
        return new Packet(typeValue, headerValues, bodyValue);
    }
}

