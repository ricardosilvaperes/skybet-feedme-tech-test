package com.skybet.feedmetechtestintermediate.model.datatype;

public class Header {

    private final int msgId;
    private final String operation;
    private final String type;
    private final long timestamp;

    public Header(final int msgId, final String operation, final String type, final long timestamp) {
        this.msgId = msgId;
        this.operation = operation;
        this.type = type;
        this.timestamp = timestamp;
    }

    public int getMsgId() {
        return msgId;
    }

    public String getOperation() {
        return operation;
    }

    public String getType() {
        return type;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
