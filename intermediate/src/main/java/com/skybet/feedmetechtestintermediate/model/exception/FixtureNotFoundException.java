package com.skybet.feedmetechtestintermediate.model.exception;

public final class FixtureNotFoundException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "fixture with id %s not found";

    public FixtureNotFoundException(final String fixtureId) {
        super(String.format(MESSAGE_TEMPLATE, fixtureId));
    }
}
