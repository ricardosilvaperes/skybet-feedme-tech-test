package com.skybet.feedmetechtestintermediate.model.datatype.market;

import com.skybet.feedmetechtestintermediate.model.datatype.DataType;
import com.skybet.feedmetechtestintermediate.model.datatype.Header;

public class Market extends DataType<MarketBody> {

    public Market(final Header header, final MarketBody marketBody) {
        super(header, marketBody);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Market{");
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
