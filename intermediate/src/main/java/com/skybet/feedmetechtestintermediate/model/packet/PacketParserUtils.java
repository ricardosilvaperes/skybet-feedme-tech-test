package com.skybet.feedmetechtestintermediate.model.packet;

import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class PacketParserUtils {

    private static String extractBodyValue(final String[] headerAndBody) {
        return headerAndBody[4];
    }

    private static String[] extractHeaderValues(final String[] headerAndBody) {
        return Arrays.copyOfRange(headerAndBody, 0, 4);
    }

    private static String[] joinTypeValueAndHeaderValuesAndBodyValue(final String[] headerValues, final String bodyValue) {
        final String[] typeValueAndHeadersValuesAndBodyValue = new String[headerValues.length + 2];
        typeValueAndHeadersValuesAndBodyValue[0] = headerValues[2];
        for (int i = 0; i < headerValues.length; i++) {
            typeValueAndHeadersValuesAndBodyValue[i + 1] = headerValues[i];
        }
        typeValueAndHeadersValuesAndBodyValue[headerValues.length + 1] = bodyValue;
        return typeValueAndHeadersValuesAndBodyValue;
    }

    private static String removeOuterPipes(final String str) {
        return str.substring(1, str.length() - 1);
    }

    private static String[] splitHeaderFromBody(final String packet) {
        return packet.split("\\|", 5);
    }

    public String[] splitPacket(final String packet) {
        final String packetWithoutOuterPipes = removeOuterPipes(packet);
        final String[] headerAndBody = splitHeaderFromBody(packetWithoutOuterPipes);
        final String[] headerValues = extractHeaderValues(headerAndBody);
        final String bodyValue = extractBodyValue(headerAndBody);
        return joinTypeValueAndHeaderValuesAndBodyValue(headerValues, bodyValue);
    }
}
