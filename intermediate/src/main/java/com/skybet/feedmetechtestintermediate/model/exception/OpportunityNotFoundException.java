package com.skybet.feedmetechtestintermediate.model.exception;

public final class OpportunityNotFoundException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "opportunity with id %s not found";

    public OpportunityNotFoundException(final String opportunityId) {
        super(String.format(MESSAGE_TEMPLATE, opportunityId));
    }
}
