package com.skybet.feedmetechtestintermediate.domain;

import java.util.Objects;

public class WinningResult {

    private final String id;
    private final String name;
    private final String price;
    private final boolean displayed;
    private final boolean suspended;

    public WinningResult(final String id, final String name, final String price, final boolean displayed, final boolean suspended) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.displayed = displayed;
        this.suspended = suspended;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        final WinningResult that = (WinningResult) other;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("WinningResult{");
        stringBuilder.append("id='").append(id).append('\'');
        stringBuilder.append(", name='").append(name).append('\'');
        stringBuilder.append(", price='").append(price).append('\'');
        stringBuilder.append(", displayed=").append(displayed);
        stringBuilder.append(", suspended=").append(suspended);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
