attempt to complete the following test: https://github.com/skybet/feedme-tech-test

## instructions:

### basic:
 - cd docker/
 - docker-compose up
 - cd ../basic/
 - mvn clean install
 - cd target/
 - java -jar feedmetechtest-basic.jar
 - received data from provider should be print into the console
 - docker-compose down

### intermediate:
 - cd docker/
 - docker-compose up
 - cd ../intermediate
 - mvn clean install
 - cd target/
 - java -jar feedmetechtest-intermediate.jar
 - using a browser, go to http://localhost:8383/swagger-ui.html and the persisted data should be available
   by making a get all request or a get one (providing the event id) request
 - docker-compose down

### advanced:
 - cd docker/
 - docker-compose up
 #### &nbsp;&nbsp;producer:
  - cd ../advanced/producer/
  - mvn clean install
  - cd target/
  - java -jar feedmetechtest-advanced-producer.jar
 #### &nbsp;&nbsp;consumer(s):
  - cd ../consumer/
  - mvn clean install
  - cd target/
  - java -jar feedmetechtest-advanced-consumer.jar &
  - java -jar feedmetechtest-advanced-consumer.jar &
 #### &nbsp;&nbsp;visualizer:
  - cd ../
  - mvn clean install
  - cd target/
  - java -jar feedmetechtest-advanced-visualizer.jar
  - using a browser, go to http://localhost:8383/swagger-ui.html and the persisted data should be available
    by making a get all request or a get one (providing the event id) request
 - docker-compose down