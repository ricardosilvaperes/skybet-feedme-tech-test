package com.skybet.feedmetechtestadvancedproducer.model.datatype.market;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class MarketBodyValidatorTest {

    private final MarketBodyValidator marketBodyValidator = new MarketBodyValidator();

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidEventId() {
        // Given
        final String data = "invalidEventId|00000000-0000-0000-0000-000000000000|name|0|1";

        // When
        final boolean result = marketBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidMarketId() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|invalidEventId|name|0|1";

        // When
        final boolean result = marketBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidName() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|00000000-0000-0000-0000-000000000000||0|1";

        // When
        final boolean result = marketBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidDisplayedValue() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|00000000-0000-0000-0000-000000000000|name|invalidDisplayedValue|1";

        // When
        final boolean result = marketBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidSuspendedValue() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|00000000-0000-0000-0000-000000000000|name|0|invalidSuspendedValue";

        // When
        final boolean result = marketBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnTrue() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|00000000-0000-0000-0000-000000000000|name|0|1";

        // When
        final boolean result = marketBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isTrue();
    }
}
