package com.skybet.feedmetechtestadvancedproducer.model.datatype.event;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataTypeParserTest;
import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataTypeParserUtils;
import com.skybet.feedmetechtestadvancedproducer.model.datatype.Header;
import com.skybet.feedmetechtestadvancedproducer.model.exception.InvalidPacketException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EventParserTest extends DataTypeParserTest {

    @Mock private DataTypeParserUtils dataTypeParserUtils;

    @Mock private EventBodyValidator eventBodyValidator;

    @InjectMocks private EventParser eventParser;

    @Test
    public void parse() {
        // Given
        final int msgId = 1;
        final String strMsgId = String.valueOf(msgId);
        final String operation = "create";
        final String type = "event";
        final long timestamp = 1L;
        final String strTimestamp = String.valueOf(timestamp);
        final String eventId = "eventId";
        final String category = "category";
        final String subCategory = "subCategory";
        final String name = "name";
        final long startTime = 1L;
        final boolean displayed = false;
        final String strDisplayed = "0";
        final boolean suspended = true;
        final String strSuspended = "1";
        final String strStartTime = String.valueOf(startTime);
        final String[] headerValues = {strMsgId, operation, type, strTimestamp};
        final String bodyValue = "bodyValue";
        final String[] eventValues = {eventId, category, subCategory, name, strStartTime, strDisplayed, strSuspended};
        when(eventBodyValidator.dataIsValid(bodyValue)).thenReturn(true);
        when(dataTypeParserUtils.splitBodyValue(bodyValue)).thenReturn(eventValues);
        when(dataTypeParserUtils.booleanValueOf(strDisplayed)).thenReturn(false);
        when(dataTypeParserUtils.booleanValueOf(strSuspended)).thenReturn(true);

        // When
        final Event actualEvent = eventParser.parse(headerValues, bodyValue);

        // Then
        assertThat(actualEvent).isNotNull();
        final Header actualEventHeader = actualEvent.getHeader();
        assertHeader(actualEventHeader, msgId, operation, type, timestamp);
        final EventBody actualEventBody = actualEvent.getBody();
        assertThat(actualEventBody.getEventId()).isEqualTo(eventId);
        assertThat(actualEventBody.getCategory()).isEqualTo(category);
        assertThat(actualEventBody.getSubCategory()).isEqualTo(subCategory);
        assertThat(actualEventBody.getName()).isEqualTo(name);
        assertThat(actualEventBody.getStartTime()).isEqualTo(startTime);
        assertThat(actualEventBody.isDisplayed()).isEqualTo(displayed);
        assertThat(actualEventBody.isSuspended()).isEqualTo(suspended);
    }

    @Test
    public void parseShouldThrowInvalidPacketException() {
        // Given
        final String[] headerValues = {};
        final String bodyValue = "bodyValue";
        when(eventBodyValidator.dataIsValid(bodyValue)).thenReturn(false);

        // When
        final Throwable throwable = catchThrowable(() -> eventParser.parse(headerValues, bodyValue));

        // Then
        assertThat(throwable)
                .isInstanceOf(InvalidPacketException.class)
                .hasMessage("invalid packet data detected: %s", bodyValue);
    }
}
