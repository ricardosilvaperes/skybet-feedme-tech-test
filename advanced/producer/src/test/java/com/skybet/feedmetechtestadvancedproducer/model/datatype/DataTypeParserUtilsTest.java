package com.skybet.feedmetechtestadvancedproducer.model.datatype;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class DataTypeParserUtilsTest {

    private final DataTypeParserUtils dataTypeParserUtils = new DataTypeParserUtils();

    @Test
    public void booleanValueOfShouldReturnFalse() {
        // Given
        final String strBool = "0";

        // When
        final boolean bool = dataTypeParserUtils.booleanValueOf(strBool);

        // Then
        assertThat(bool).isFalse();
    }

    @Test
    public void booleanValueOfShouldReturnTrue() {
        // Given
        final String strBool = "1";

        // When
        final boolean bool = dataTypeParserUtils.booleanValueOf(strBool);

        // Then
        assertThat(bool).isTrue();
    }

    @Test
    public void splitBodyValue() {
        // Given
        final String valueA = "a";
        final String valueB = "b";
        final String valueC = "c";
        final String bodyValue = valueA + "\\||\\|" + valueB + "\\||\\|" + valueC;

        // When
        final String[] splitBodyValue = dataTypeParserUtils.splitBodyValue(bodyValue);

        // Then
        assertThat(splitBodyValue).containsExactly(valueA, valueB, valueC);
    }
}
