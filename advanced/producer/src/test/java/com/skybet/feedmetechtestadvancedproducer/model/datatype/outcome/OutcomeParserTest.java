package com.skybet.feedmetechtestadvancedproducer.model.datatype.outcome;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataTypeParserTest;
import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataTypeParserUtils;
import com.skybet.feedmetechtestadvancedproducer.model.datatype.Header;
import com.skybet.feedmetechtestadvancedproducer.model.exception.InvalidPacketException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OutcomeParserTest extends DataTypeParserTest {

    @Mock private DataTypeParserUtils dataTypeParserUtils;

    @Mock private OutcomeBodyValidator outcomeBodyValidator;

    @InjectMocks private OutcomeParser outcomeParser;

    @Test
    public void parse() {
        // Given
        final int msgId = 1;
        final String strMsgId = String.valueOf(msgId);
        final String operation = "create";
        final String type = "outcome";
        final long timestamp = 1L;
        final String strTimestamp = String.valueOf(timestamp);
        final String marketId = "marketId";
        final String outcomeId = "outcomeId";
        final String name = "name";
        final String price = "price";
        final boolean displayed = false;
        final String strDisplayed = "0";
        final boolean suspended = true;
        final String strSuspended = "1";
        final String[] headerValues = {strMsgId, operation, type, strTimestamp};
        final String bodyValue = "bodyValue";
        final String[] outcomeValues = {marketId, outcomeId, name, price, strDisplayed, strSuspended};
        when(outcomeBodyValidator.dataIsValid(bodyValue)).thenReturn(true);
        when(dataTypeParserUtils.splitBodyValue(bodyValue)).thenReturn(outcomeValues);
        when(dataTypeParserUtils.booleanValueOf(strDisplayed)).thenReturn(false);
        when(dataTypeParserUtils.booleanValueOf(strSuspended)).thenReturn(true);

        // When
        final Outcome actualOutcome = outcomeParser.parse(headerValues, bodyValue);

        // Then
        assertThat(actualOutcome).isNotNull();
        final Header actualOutcomeHeader = actualOutcome.getHeader();
        assertHeader(actualOutcomeHeader, msgId, operation, type, timestamp);
        final OutcomeBody actualOutcomeBody = actualOutcome.getBody();
        assertThat(actualOutcomeBody.getMarketId()).isEqualTo(marketId);
        assertThat(actualOutcomeBody.getOutcomeId()).isEqualTo(outcomeId);
        assertThat(actualOutcomeBody.getName()).isEqualTo(name);
        assertThat(actualOutcomeBody.getPrice()).isEqualTo(price);
        assertThat(actualOutcomeBody.isDisplayed()).isEqualTo(displayed);
        assertThat(actualOutcomeBody.isSuspended()).isEqualTo(suspended);
    }

    @Test
    public void parseShouldThrowInvalidPacketException() {
        // Given
        final String[] headerValues = {};
        final String bodyValue = "bodyValue";
        when(outcomeBodyValidator.dataIsValid(bodyValue)).thenReturn(false);

        // When
        final Throwable throwable = catchThrowable(() -> outcomeParser.parse(headerValues, bodyValue));

        // Then
        assertThat(throwable)
                .isInstanceOf(InvalidPacketException.class)
                .hasMessage("invalid packet data detected: %s", bodyValue);
    }
}
