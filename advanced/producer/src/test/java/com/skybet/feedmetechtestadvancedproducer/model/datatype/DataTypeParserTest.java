package com.skybet.feedmetechtestadvancedproducer.model.datatype;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class DataTypeParserTest {

    protected void assertHeader(final Header actualHeader, final int msgId, final String operation, final String type, final long timestamp) {
        assertThat(actualHeader).isNotNull();
        assertThat(actualHeader.getMsgId()).isEqualTo(msgId);
        assertThat(actualHeader.getOperation()).isEqualTo(operation);
        assertThat(actualHeader.getType()).isEqualTo(type);
        assertThat(actualHeader.getTimestamp()).isEqualTo(timestamp);
    }

    protected abstract void parse();
}
