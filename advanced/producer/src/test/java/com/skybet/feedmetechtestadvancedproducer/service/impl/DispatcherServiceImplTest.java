package com.skybet.feedmetechtestadvancedproducer.service.impl;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataType;
import com.skybet.feedmetechtestadvancedproducer.service.DispatcherServiceImpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

@RunWith(MockitoJUnitRunner.class)
public class DispatcherServiceImplTest {

    @Mock private RabbitTemplate rabbitTemplate;

    @InjectMocks private DispatcherServiceImpl dispatcherServiceImpl;

    @Test
    public void dispatchDataType() {
        // Given
        final DataType dataType = mock(DataType.class, CALLS_REAL_METHODS);

        // When
        dispatcherServiceImpl.dispatchDataType(dataType);

        // Then
        verify(rabbitTemplate).convertAndSend(anyString(), anyString(), eq(dataType));
    }
}
