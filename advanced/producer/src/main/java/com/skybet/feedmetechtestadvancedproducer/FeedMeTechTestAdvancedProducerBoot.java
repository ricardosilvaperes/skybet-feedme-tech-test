package com.skybet.feedmetechtestadvancedproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedMeTechTestAdvancedProducerBoot {

    public static void main(String[] args) {
        SpringApplication.run(FeedMeTechTestAdvancedProducerBoot.class, args);
    }
}
