package com.skybet.feedmetechtestadvancedproducer.service.impl;

import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataType;

public interface DispatcherService {

    void dispatchDataType(final DataType dataType);
}
