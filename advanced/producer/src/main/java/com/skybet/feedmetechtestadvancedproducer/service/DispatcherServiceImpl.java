package com.skybet.feedmetechtestadvancedproducer.service;

import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataType;
import com.skybet.feedmetechtestadvancedproducer.service.impl.DispatcherService;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DispatcherServiceImpl implements DispatcherService {

    private final String exchange;
    private final String routingKey;
    private final RabbitTemplate rabbitTemplate;

    public DispatcherServiceImpl(@Value("${feedmetechtest.messaging.data-type.exchange:feedmetechtest.ex}") final String exchange,
            @Value("${feedmetechtest.messaging.data-type.routing-key:feedmetechtest.dataType}") final String routingKey,
            final RabbitTemplate rabbitTemplate)
    {
        this.exchange = exchange;
        this.routingKey = routingKey;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void dispatchDataType(final DataType dataType) {
        rabbitTemplate.convertAndSend(exchange, routingKey, dataType);
    }
}
