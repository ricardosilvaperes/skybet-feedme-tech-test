package com.skybet.feedmetechtestadvancedproducer.model.datatype;

import java.util.regex.Pattern;

public abstract class DataTypeBodyValidator {

    protected static final String UUID_REGEX = "[a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12}";

    private final Pattern pattern;

    protected DataTypeBodyValidator(final String dataTypePatternRegex) {
        pattern = Pattern.compile(dataTypePatternRegex);
    }

    public boolean dataIsValid(final String data) {
        return pattern.matcher(data).matches();
    }
}
