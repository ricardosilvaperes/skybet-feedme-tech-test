package com.skybet.feedmetechtestadvancedproducer.provider;

import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataType;
import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataTypeParser;
import com.skybet.feedmetechtestadvancedproducer.model.packet.Packet;
import com.skybet.feedmetechtestadvancedproducer.model.packet.PacketParser;
import com.skybet.feedmetechtestadvancedproducer.service.impl.DispatcherService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.Executors;

@Component
public class ProviderClient implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProviderClient.class);

    private final String ip;
    private final int port;
    private final DispatcherService dispatcherService;
    private final PacketParser packetParser;
    private final Map<String, DataTypeParser> dataTypeParsers;
    private short connAttempts;

    public ProviderClient(@Value("${feedmetechtest-provider.ip:127.0.0.1}") final String ip,
            @Value("${feedmetechtest-provider.port:8282}") final int port,
            @Value("${feedmetechtest-provider.conn-attempts:5}") final short connAttempts,
            final DispatcherService dispatcherService,
            final PacketParser packetParser,
            final Map<String, DataTypeParser> dataTypeParsers)
    {
        this.ip = ip;
        this.port = port;
        this.dispatcherService = dispatcherService;
        this.connAttempts = connAttempts;
        this.packetParser = packetParser;
        this.dataTypeParsers = dataTypeParsers;
    }

    private static void verifyIfShouldContinue(final int currentNumOfAttempts, final String message) {
        if (currentNumOfAttempts > 0) {
            LOGGER.warn(message);
            try {
                Thread.sleep(3000);
            } catch (final InterruptedException interruptedException) {

            }
        } else {
            LOGGER.error("max number of connection attempts reached... is the provider alive? Bye!");
            System.exit(-1);
        }
    }

    public void connect() {
        short tmpCurrentNumOfAttempts = connAttempts;
        while (tmpCurrentNumOfAttempts > 0) {
            try (Socket socket = new Socket(ip, port);
                    Reader reader = new InputStreamReader(socket.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(reader))
            {
                while (true) {
                    try {
                        final String data = bufferedReader.readLine();
                        final Packet packet = packetParser.parse(data);
                        final String type = packet.getTypeValue();
                        final DataTypeParser dataTypeParser = dataTypeParsers.get(type);
                        if (dataTypeParser == null) {
                            LOGGER.warn("unknown packet type detected -> '{}':", type);
                        } else {
                            final DataType dataType = dataTypeParser.parse(packet.getHeaderValues(), packet.getBodyValue());
                            dispatcherService.dispatchDataType(dataType);
                        }
                    } catch (final Exception exception) {
                        LOGGER.error("error receiving data, details: {}", exception.getMessage());
                        tmpCurrentNumOfAttempts--;
                        verifyIfShouldContinue(tmpCurrentNumOfAttempts, "connection with provider will be restarted...");
                        break;
                    }
                }
            } catch (final IOException ioException) {
                LOGGER.error("error trying connecting to provider, details: {}", ioException.getMessage());
                tmpCurrentNumOfAttempts--;
                verifyIfShouldContinue(tmpCurrentNumOfAttempts, "new connection with provider will be attempted...");
            }
        }
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent applicationReadyEvent) {
        Executors.newSingleThreadExecutor()
                .execute(this::connect);
    }
}
