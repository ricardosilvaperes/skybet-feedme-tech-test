package com.skybet.feedmetechtestadvancedproducer.model.datatype.outcome;

import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataTypeBodyValidator;

import org.springframework.stereotype.Component;

@Component
public class OutcomeBodyValidator extends DataTypeBodyValidator {

    private static final String OUTCOME_PATTERN_REGEX = UUID_REGEX + "\\|" + UUID_REGEX + "\\|.+\\|\\d+/\\d+\\|(0|1)\\|(0|1)";

    public OutcomeBodyValidator() {
        super(OUTCOME_PATTERN_REGEX);
    }
}
