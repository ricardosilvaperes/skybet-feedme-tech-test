package com.skybet.feedmetechtestadvancedproducer.model.datatype.outcome;

import com.skybet.feedmetechtestadvancedproducer.model.datatype.DataType;
import com.skybet.feedmetechtestadvancedproducer.model.datatype.Header;

public final class Outcome extends DataType<OutcomeBody> {

    public Outcome(final Header header, final OutcomeBody outcomeBody) {
        super(header, outcomeBody);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Outcome{");
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
