package com.skybet.feedmetechtestadvancedvisualizer.controller;

import com.skybet.feedmetechtestadvancedvisualizer.domain.Fixture;
import com.skybet.feedmetechtestadvancedvisualizer.repository.FixtureRepository;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/fixture")
public class FixtureController {

    private final FixtureRepository fixtureRepository;

    public FixtureController(final FixtureRepository fixtureRepository) {
        this.fixtureRepository = fixtureRepository;
    }

    @GetMapping("/all")
    public List<Fixture> getAll() {
        return fixtureRepository.findAll();
    }

    @GetMapping("/one")
    public Fixture getOne(@RequestParam final String id) {
        return fixtureRepository.findOne(id);
    }
}
