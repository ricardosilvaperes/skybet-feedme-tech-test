package com.skybet.feedmetechtestadvancedvisualizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedMeTechTestAdvancedVisualizerBoot {

    public static void main(String[] args) {
        SpringApplication.run(FeedMeTechTestAdvancedVisualizerBoot.class, args);
    }
}
