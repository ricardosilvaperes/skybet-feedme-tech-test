package com.skybet.feedmetechtestadvancedvisualizer.repository;

import com.skybet.feedmetechtestadvancedvisualizer.domain.Fixture;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FixtureRepository extends MongoRepository<Fixture, String> {

}
