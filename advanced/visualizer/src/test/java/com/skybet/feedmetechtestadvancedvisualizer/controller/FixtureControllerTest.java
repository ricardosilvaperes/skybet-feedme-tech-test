package com.skybet.feedmetechtestadvancedvisualizer.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestadvancedvisualizer.domain.Fixture;
import com.skybet.feedmetechtestadvancedvisualizer.repository.FixtureRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class FixtureControllerTest {

    @Mock private FixtureRepository fixtureRepository;

    @InjectMocks private FixtureController fixtureController;

    @Test
    public void getAll() {
        // Given
        final Fixture fixture = mock(Fixture.class);
        final List<Fixture> expectedFixtures = Collections.singletonList(fixture);
        when(fixtureRepository.findAll()).thenReturn(expectedFixtures);

        // When
        final List<Fixture> actualFixtures = fixtureController.getAll();

        // Then
        verify(fixtureRepository).findAll();
        assertThat(actualFixtures).isEqualTo(expectedFixtures);
    }

    @Test
    public void getOne() {
        // Given
        final String id = "id";
        final Fixture expectedFixture = mock(Fixture.class);
        when(fixtureRepository.findOne(id)).thenReturn(expectedFixture);

        // When
        final Fixture actualFixture = fixtureController.getOne(id);

        // Then
        verify(fixtureRepository).findOne(id);
        assertThat(actualFixture).isEqualTo(expectedFixture);
    }
}
