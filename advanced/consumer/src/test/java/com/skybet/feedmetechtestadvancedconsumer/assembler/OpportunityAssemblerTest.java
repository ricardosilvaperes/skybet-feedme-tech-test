package com.skybet.feedmetechtestadvancedconsumer.assembler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestadvancedconsumer.domain.Opportunity;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Header;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.market.Market;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.market.MarketBody;

import org.junit.Test;

public class OpportunityAssemblerTest {

    private final OpportunityAssembler opportunityAssembler = new OpportunityAssembler();

    @Test
    public void mapFrom() {
        // Given
        final String id = "id";
        final String name = "name";
        final boolean displayed = true;
        final boolean suspended = false;
        final Header header = mock(Header.class);
        final MarketBody marketBody = mock(MarketBody.class);
        final Market market = new Market(header, marketBody);
        when(marketBody.getMarketId()).thenReturn(id);
        when(marketBody.getName()).thenReturn(name);
        when(marketBody.isDisplayed()).thenReturn(displayed);
        when(marketBody.isSuspended()).thenReturn(suspended);

        // When
        final Opportunity actualOpportunity = opportunityAssembler.mapFrom(market);

        // Then
        assertThat(actualOpportunity).isNotNull();
        assertThat(actualOpportunity.getId()).isEqualTo(id);
        assertThat(actualOpportunity.getName()).isEqualTo(name);
        assertThat(actualOpportunity.isDisplayed()).isEqualTo(displayed);
        assertThat(actualOpportunity.isSuspended()).isEqualTo(suspended);
    }
}
