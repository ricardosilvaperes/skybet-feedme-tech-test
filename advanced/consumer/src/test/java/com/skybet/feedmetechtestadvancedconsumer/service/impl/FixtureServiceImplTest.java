package com.skybet.feedmetechtestadvancedconsumer.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestadvancedconsumer.assembler.FixtureAssembler;
import com.skybet.feedmetechtestadvancedconsumer.assembler.OpportunityAssembler;
import com.skybet.feedmetechtestadvancedconsumer.assembler.WinningResultAssembler;
import com.skybet.feedmetechtestadvancedconsumer.domain.Fixture;
import com.skybet.feedmetechtestadvancedconsumer.domain.Opportunity;
import com.skybet.feedmetechtestadvancedconsumer.domain.WinningResult;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Body;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.DataType;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Header;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.event.Event;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.event.EventBody;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.market.Market;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.market.MarketBody;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.outcome.Outcome;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.outcome.OutcomeBody;
import com.skybet.feedmetechtestadvancedconsumer.model.exception.FixtureNotFoundException;
import com.skybet.feedmetechtestadvancedconsumer.model.exception.OpportunityNotFoundException;
import com.skybet.feedmetechtestadvancedconsumer.repository.FixtureRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@RunWith(MockitoJUnitRunner.class)
public class FixtureServiceImplTest {

    private static final int MSG_ID = 1;

    private FixtureAssembler fixtureAssembler = mock(FixtureAssembler.class);

    private OpportunityAssembler opportunityAssembler = mock(OpportunityAssembler.class);

    private WinningResultAssembler winningResultAssembler = mock(WinningResultAssembler.class);

    private FixtureRepository fixtureRepository = mock(FixtureRepository.class);

    private final FixtureServiceImpl fixtureServiceImpl;

    public FixtureServiceImplTest() {
        final RetryTemplate retryTemplate = new RetryTemplate();
        retryTemplate.setRetryPolicy(new NeverRetryPolicy());
        fixtureServiceImpl = new FixtureServiceImpl(fixtureAssembler, opportunityAssembler, winningResultAssembler, fixtureRepository, retryTemplate);
    }

    @Test
    public void handleDataTypeShouldThrowIllegalArgumentExceptionDueToUnknownDataType() {
        // Given
        class UnknownDataType extends DataType {

            public UnknownDataType(final Header header, final Body body) {
                super(header, body);
            }
        }
        final Header header = mock(Header.class);
        final UnknownDataType unknownDataType = mock(UnknownDataType.class);
        when(header.getMsgId()).thenReturn(MSG_ID);
        when(unknownDataType.getHeader()).thenReturn(header);

        // When
        final Throwable throwable = catchThrowable(() -> fixtureServiceImpl.handleDataType(unknownDataType));

        // Then
        assertThat(throwable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("behaviour not implemented for %s", unknownDataType.getClass().getSimpleName());
    }

    @Test
    public void handleCreateEvent() {
        // Given
        final String operation = "create";
        final Header header = mock(Header.class);
        final Event event = mock(Event.class);
        final Fixture fixture = mock(Fixture.class);
        when(header.getMsgId()).thenReturn(MSG_ID);
        when(header.getOperation()).thenReturn(operation);
        when(event.getHeader()).thenReturn(header);
        when(fixtureAssembler.mapFrom(event)).thenReturn(fixture);

        // When
        fixtureServiceImpl.handleDataType(event);

        // Then
        verify(fixtureAssembler).mapFrom(event);
        verify(fixtureRepository).save(fixture);
    }

    @Test
    public void handleUpdateEvent() {
        // Given
        final String operation = "update";
        final String id = "id";
        final Header header = mock(Header.class);
        final EventBody eventBody = mock(EventBody.class);
        final Event event = mock(Event.class);
        final Fixture fixture = mock(Fixture.class);
        when(header.getMsgId()).thenReturn(MSG_ID);
        when(header.getOperation()).thenReturn(operation);
        when(eventBody.getEventId()).thenReturn(id);
        when(event.getHeader()).thenReturn(header);
        when(event.getBody()).thenReturn(eventBody);
        when(fixtureRepository.findOne(id)).thenReturn(fixture);

        // When
        fixtureServiceImpl.handleDataType(event);

        // Then
        verify(fixtureRepository).findOne(id);
        verify(fixtureAssembler).update(fixture, event);
        verify(fixtureRepository).save(fixture);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void handleUpdateEventShouldThrowFixtureNotFoundException() {
        // Given
        final String operation = "update";
        final String id = "id";
        final Header header = mock(Header.class);
        final EventBody eventBody = mock(EventBody.class);
        final Event event = mock(Event.class);
        when(header.getMsgId()).thenReturn(MSG_ID);
        when(header.getOperation()).thenReturn(operation);
        when(eventBody.getEventId()).thenReturn(id);
        when(event.getHeader()).thenReturn(header);
        when(event.getBody()).thenReturn(eventBody);
        when(fixtureRepository.findOne(id)).thenThrow(new FixtureNotFoundException(id));

        // When
        final Throwable throwable = catchThrowable(() -> fixtureServiceImpl.handleDataType(event));

        // Then
        verify(fixtureRepository).findOne(id);
        verify(fixtureAssembler, never()).update(any(Fixture.class), eq(event));
        verify(fixtureRepository, never()).save(any(Fixture.class));
        assertThat(throwable)
                .isInstanceOf(FixtureNotFoundException.class)
                .hasMessage("fixture with id %s not found", id);
    }

    @Test
    public void handleMarket() {
        // Given
        final String id = "id";
        final Header header = mock(Header.class);
        final MarketBody marketBody = mock(MarketBody.class);
        final Market market = mock(Market.class);
        final Opportunity opportunity = mock(Opportunity.class);
        final Fixture fixture = mock(Fixture.class);
        when(header.getMsgId()).thenReturn(MSG_ID);
        when(marketBody.getEventId()).thenReturn(id);
        when(market.getHeader()).thenReturn(header);
        when(market.getBody()).thenReturn(marketBody);
        when(opportunityAssembler.mapFrom(market)).thenReturn(opportunity);
        when(fixtureRepository.findOne(id)).thenReturn(fixture);

        // When
        fixtureServiceImpl.handleDataType(market);

        // Then
        verify(opportunityAssembler).mapFrom(market);
        verify(fixtureRepository).findOne(id);
        verify(fixture).addOpportunity(opportunity);
        verify(fixtureRepository).save(fixture);
    }

    @Test
    public void handleMarketShouldThrowFixtureNotFoundException() {
        // Given
        final String id = "id";
        final Header header = mock(Header.class);
        final MarketBody marketBody = mock(MarketBody.class);
        final Market market = mock(Market.class);
        when(header.getMsgId()).thenReturn(MSG_ID);
        when(marketBody.getEventId()).thenReturn(id);
        when(market.getHeader()).thenReturn(header);
        when(market.getBody()).thenReturn(marketBody);
        when(fixtureRepository.findOne(id)).thenThrow(new FixtureNotFoundException(id));

        // When
        final Throwable throwable = catchThrowable(() -> fixtureServiceImpl.handleDataType(market));

        // Then
        verify(opportunityAssembler).mapFrom(market);
        verify(fixtureRepository).findOne(id);
        verify(fixtureRepository, never()).save(any(Fixture.class));
        assertThat(throwable)
                .isInstanceOf(FixtureNotFoundException.class)
                .hasMessage("fixture with id %s not found", id);
    }

    @Test
    public void handleOutcome() {
        // Given
        final String id = "id";
        final Header header = mock(Header.class);
        final OutcomeBody outcomeBody = mock(OutcomeBody.class);
        final Outcome outcome = mock(Outcome.class);
        final WinningResult winningResult = mock(WinningResult.class);
        final Opportunity opportunity = mock(Opportunity.class);
        final Fixture fixture = mock(Fixture.class);
        when(header.getMsgId()).thenReturn(MSG_ID);
        when(outcomeBody.getMarketId()).thenReturn(id);
        when(outcome.getHeader()).thenReturn(header);
        when(outcome.getBody()).thenReturn(outcomeBody);
        when(fixture.getOpportunity(id)).thenReturn(opportunity);
        when(winningResultAssembler.mapFrom(outcome)).thenReturn(winningResult);
        when(fixtureRepository.findContainingOpportunityWithId(id)).thenReturn(fixture);

        // When
        fixtureServiceImpl.handleDataType(outcome);

        // Then
        verify(winningResultAssembler).mapFrom(outcome);
        verify(fixtureRepository).findContainingOpportunityWithId(id);
        verify(opportunity).addWinningResult(winningResult);
        verify(fixture).addOpportunity(opportunity);
        verify(fixtureRepository).save(fixture);
    }

    @Test
    public void handleOutcomeShouldThrowOpportunityNotFoundException() {
        // Given
        final String id = "id";
        final Header header = mock(Header.class);
        final OutcomeBody outcomeBody = mock(OutcomeBody.class);
        final Outcome outcome = mock(Outcome.class);
        when(header.getMsgId()).thenReturn(MSG_ID);
        when(outcomeBody.getMarketId()).thenReturn(id);
        when(outcome.getHeader()).thenReturn(header);
        when(outcome.getBody()).thenReturn(outcomeBody);
        when(fixtureRepository.findContainingOpportunityWithId(id)).thenThrow(new OpportunityNotFoundException(id));

        // When
        final Throwable throwable = catchThrowable(() -> fixtureServiceImpl.handleDataType(outcome));

        // Then
        verify(winningResultAssembler).mapFrom(outcome);
        verify(fixtureRepository).findContainingOpportunityWithId(id);
        verify(fixtureRepository, never()).save(any(Fixture.class));
        assertThat(throwable)
                .isInstanceOf(OpportunityNotFoundException.class)
                .hasMessage("opportunity with id %s not found", id);
    }
}

