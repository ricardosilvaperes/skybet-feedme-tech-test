package com.skybet.feedmetechtestadvancedconsumer.assembler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.skybet.feedmetechtestadvancedconsumer.domain.WinningResult;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Header;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.outcome.Outcome;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.outcome.OutcomeBody;

import org.junit.Test;

public class WinningResultAssemblerTest {

    private final WinningResultAssembler winningResultAssembler = new WinningResultAssembler();

    @Test
    public void mapFrom() {
        // Given
        final String id = "id";
        final String name = "name";
        final String price = "price";
        final boolean displayed = true;
        final boolean suspended = false;
        final Header header = mock(Header.class);
        final OutcomeBody outcomeBody = mock(OutcomeBody.class);
        final Outcome outcome = new Outcome(header, outcomeBody);
        when(outcomeBody.getOutcomeId()).thenReturn(id);
        when(outcomeBody.getName()).thenReturn(name);
        when(outcomeBody.getPrice()).thenReturn(price);
        when(outcomeBody.isDisplayed()).thenReturn(displayed);
        when(outcomeBody.isSuspended()).thenReturn(suspended);

        // When
        final WinningResult actualWinningResult = winningResultAssembler.mapFrom(outcome);

        // Then
        assertThat(actualWinningResult).isNotNull();
        assertThat(actualWinningResult.getId()).isEqualTo(id);
        assertThat(actualWinningResult.getName()).isEqualTo(name);
        assertThat(actualWinningResult.getPrice()).isEqualTo(price);
        assertThat(actualWinningResult.isDisplayed()).isEqualTo(displayed);
        assertThat(actualWinningResult.isSuspended()).isEqualTo(suspended);
    }
}
