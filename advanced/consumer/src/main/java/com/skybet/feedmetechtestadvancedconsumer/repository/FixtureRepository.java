package com.skybet.feedmetechtestadvancedconsumer.repository;

import com.skybet.feedmetechtestadvancedconsumer.domain.Fixture;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FixtureRepository extends MongoRepository<Fixture, String> {

    @Query("{ 'opportunities.id' : ?0 }")
    Fixture findContainingOpportunityWithId(final String opportunityId);
}
