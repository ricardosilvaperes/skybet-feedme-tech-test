package com.skybet.feedmetechtestadvancedconsumer.config;

import com.skybet.feedmetechtestadvancedconsumer.model.exception.FixtureNotFoundException;
import com.skybet.feedmetechtestadvancedconsumer.model.exception.OpportunityNotFoundException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.util.HashMap;
import java.util.Map;

@EnableRetry
@Configuration
public class RetryConfig {

    @Bean
    public RetryTemplate retryTemplate() {
        final RetryTemplate template = new RetryTemplate();
        final Map<Class<? extends Throwable>, Boolean> retryableExceptions = new HashMap<>(3);
        retryableExceptions.put(FixtureNotFoundException.class, true);
        retryableExceptions.put(OpportunityNotFoundException.class, true);
        retryableExceptions.put(OptimisticLockingFailureException.class, true);
        final SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(5, retryableExceptions);
        final FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(2000);
        template.setRetryPolicy(retryPolicy);
        template.setBackOffPolicy(backOffPolicy);
        return template;
    }
}
