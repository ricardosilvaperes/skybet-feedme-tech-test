package com.skybet.feedmetechtestadvancedconsumer.model.datatype;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.event.Event;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.market.Market;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.outcome.Outcome;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = Event.class, name = "event"),
    @JsonSubTypes.Type(value = Market.class, name = "market"),
    @JsonSubTypes.Type(value = Outcome.class, name = "outcome")
})
public abstract class DataType<T extends Body> {

    private Header header;
    private T body;

    protected DataType() {

    }

    protected DataType(final Header header, final T body) {
        this.header = header;
        this.body = body;
    }

    public Header getHeader() {
        return header;
    }

    public T getBody() {
        return body;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("header=").append(header);
        stringBuilder.append(", body=").append(body);
        return stringBuilder.toString();
    }
}
