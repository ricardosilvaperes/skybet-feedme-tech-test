package com.skybet.feedmetechtestadvancedconsumer.domain;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Opportunity {

    private final String id;
    private final String name;
    private final Set<WinningResult> winningResults = new HashSet<>();
    private final boolean displayed;
    private final boolean suspended;

    public Opportunity(final String id, final String name, final boolean displayed, final boolean suspended) {
        this.id = id;
        this.name = name;
        this.displayed = displayed;
        this.suspended = suspended;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<WinningResult> getWinningResults() {
        return winningResults;
    }

    public WinningResult getWinningResult(final String winningResultId) {
        return winningResults.stream()
                .filter(winningResult -> winningResult.getId().equals(winningResultId))
                .findFirst()
                .orElse(null);
    }

    public void addWinningResult(final WinningResult winningResult) {
        winningResults.add(winningResult);
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        final Opportunity that = (Opportunity) other;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Opportunity{");
        stringBuilder.append("id='").append(id).append('\'');
        stringBuilder.append(", name='").append(name).append('\'');
        stringBuilder.append(", displayed=").append(displayed);
        stringBuilder.append(", suspended=").append(suspended);
        stringBuilder.append(", winningResults=").append(winningResults);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
