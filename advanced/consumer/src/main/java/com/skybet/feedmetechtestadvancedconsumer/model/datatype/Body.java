package com.skybet.feedmetechtestadvancedconsumer.model.datatype;

public abstract class Body {

    private boolean displayed;
    private boolean suspended;

    protected Body() {

    }

    protected Body(final boolean displayed, final boolean suspended) {
        this.displayed = displayed;
        this.suspended = suspended;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(", displayed=").append(displayed);
        stringBuilder.append(", suspended=").append(suspended);
        return stringBuilder.toString();
    }
}
