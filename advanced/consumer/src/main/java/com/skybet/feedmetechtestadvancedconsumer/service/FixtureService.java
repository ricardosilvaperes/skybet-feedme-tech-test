package com.skybet.feedmetechtestadvancedconsumer.service;

import com.skybet.feedmetechtestadvancedconsumer.model.datatype.DataType;

public interface FixtureService {

    void handleDataType(final DataType dataType);
}
