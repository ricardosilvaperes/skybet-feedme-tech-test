package com.skybet.feedmetechtestadvancedconsumer.model.datatype.outcome;

import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Body;

public class OutcomeBody extends Body {

    private String marketId;
    private String outcomeId;
    private String name;
    private String price;

    protected OutcomeBody() {

    }

    public OutcomeBody(final String marketId, final String outcomeId, final String name, final String price, final boolean displayed, final boolean suspended) {
        super(displayed, suspended);
        this.marketId = marketId;
        this.outcomeId = outcomeId;
        this.name = name;
        this.price = price;
    }

    public String getMarketId() {
        return marketId;
    }

    public String getOutcomeId() {
        return outcomeId;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{marketId='").append(marketId).append('\'');
        stringBuilder.append(", outcomeId='").append(outcomeId).append('\'');
        stringBuilder.append(", name='").append(name).append('\'');
        stringBuilder.append(", price='").append(price).append('\'');
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
