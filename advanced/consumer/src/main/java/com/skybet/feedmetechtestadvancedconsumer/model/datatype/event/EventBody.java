package com.skybet.feedmetechtestadvancedconsumer.model.datatype.event;

import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Body;

public class EventBody extends Body {

    private String eventId;
    private String category;
    private String subCategory;
    private String name;
    private long startTime;

    protected EventBody() {

    }

    public EventBody(final String eventId,
            final String category,
            final String subCategory,
            final String name,
            final long startTime,
            final boolean displayed,
            final boolean suspended)
    {
        super(displayed, suspended);
        this.eventId = eventId;
        this.category = category;
        this.subCategory = subCategory;
        this.name = name;
        this.startTime = startTime;
    }

    public String getEventId() {
        return eventId;
    }

    public String getCategory() {
        return category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public String getName() {
        return name;
    }

    public long getStartTime() {
        return startTime;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{eventId='").append(eventId).append('\'');
        stringBuilder.append(", category='").append(category).append('\'');
        stringBuilder.append(", subCategory='").append(subCategory).append('\'');
        stringBuilder.append(", name='").append(name).append('\'');
        stringBuilder.append(", startTime=").append(startTime);
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
