package com.skybet.feedmetechtestadvancedconsumer.service.impl;

import com.skybet.feedmetechtestadvancedconsumer.assembler.FixtureAssembler;
import com.skybet.feedmetechtestadvancedconsumer.assembler.OpportunityAssembler;
import com.skybet.feedmetechtestadvancedconsumer.assembler.WinningResultAssembler;
import com.skybet.feedmetechtestadvancedconsumer.domain.Fixture;
import com.skybet.feedmetechtestadvancedconsumer.domain.Opportunity;
import com.skybet.feedmetechtestadvancedconsumer.domain.WinningResult;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.DataType;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.event.Event;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.market.Market;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.outcome.Outcome;
import com.skybet.feedmetechtestadvancedconsumer.model.exception.FixtureNotFoundException;
import com.skybet.feedmetechtestadvancedconsumer.model.exception.OpportunityNotFoundException;
import com.skybet.feedmetechtestadvancedconsumer.repository.FixtureRepository;
import com.skybet.feedmetechtestadvancedconsumer.service.FixtureService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FixtureServiceImpl implements FixtureService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FixtureServiceImpl.class);

    private static final String UNKNOWN_DATA_TYPE_MESSAGE_TEMPLATE = "behaviour not implemented for %s";

    private final FixtureAssembler fixtureAssembler;
    private final OpportunityAssembler opportunityAssembler;
    private final WinningResultAssembler winningResultAssembler;
    private final FixtureRepository fixtureRepository;
    private final RetryTemplate retryTemplate;

    public FixtureServiceImpl(final FixtureAssembler fixtureAssembler,
            final OpportunityAssembler opportunityAssembler,
            final WinningResultAssembler winningResultAssembler,
            final FixtureRepository fixtureRepository,
            final RetryTemplate retryTemplate)
    {
        this.fixtureAssembler = fixtureAssembler;
        this.opportunityAssembler = opportunityAssembler;
        this.winningResultAssembler = winningResultAssembler;
        this.fixtureRepository = fixtureRepository;
        this.retryTemplate = retryTemplate;
    }

    @Override
    @RabbitListener(queues = "${feedmetechtest.messaging.data-type.queue:feedmetechtest.dataType}")
    public void handleDataType(final DataType dataType) {
        LOGGER.debug("handling msg {}", dataType.getHeader().getMsgId());
        retryTemplate.execute(retryContext -> {
            if (dataType instanceof Event) {
                handleEvent((Event) dataType);
            } else if (dataType instanceof Market) {
                handleMarket((Market) dataType);
            } else if (dataType instanceof Outcome) {
                handleOutcome((Outcome) dataType);
            } else {
                throw new IllegalArgumentException(String.format(UNKNOWN_DATA_TYPE_MESSAGE_TEMPLATE, dataType.getClass().getSimpleName()));
            }
            return null;
        });
    }

    private void handleEvent(final Event event) {
        final String operation = event.getHeader().getOperation();
        Fixture fixture = null;
        if ("create".equals(operation)) {
            fixture = fixtureAssembler.mapFrom(event);
        } else if ("update".equals(operation)) {
            final String fixtureId = event.getBody().getEventId();
            fixture = Optional.ofNullable(fixtureRepository.findOne(fixtureId))
                    .orElseThrow(() -> new FixtureNotFoundException(fixtureId));
            fixtureAssembler.update(fixture, event);
        }
        fixtureRepository.save(fixture);
    }

    private void handleMarket(final Market market) {
        final String fixtureId = market.getBody().getEventId();
        final Opportunity opportunity = opportunityAssembler.mapFrom(market);
        final Fixture fixture = Optional.ofNullable(fixtureRepository.findOne(fixtureId))
                .orElseThrow(() -> new FixtureNotFoundException(fixtureId));
        fixture.addOpportunity(opportunity);
        fixtureRepository.save(fixture);
    }

    private void handleOutcome(final Outcome outcome) {
        final String opportunityId = outcome.getBody().getMarketId();
        final WinningResult winningResult = winningResultAssembler.mapFrom(outcome);
        final Fixture fixture = Optional.ofNullable(fixtureRepository.findContainingOpportunityWithId(opportunityId))
                .orElseThrow(() -> new OpportunityNotFoundException(opportunityId));
        final Opportunity opportunity = fixture.getOpportunity(opportunityId);
        opportunity.addWinningResult(winningResult);
        fixture.addOpportunity(opportunity);
        fixtureRepository.save(fixture);
    }
}
