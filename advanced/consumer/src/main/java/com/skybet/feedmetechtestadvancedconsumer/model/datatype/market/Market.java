package com.skybet.feedmetechtestadvancedconsumer.model.datatype.market;

import com.skybet.feedmetechtestadvancedconsumer.model.datatype.DataType;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Header;

public class Market extends DataType<MarketBody> {

    protected Market() {

    }

    public Market(final Header header, final MarketBody marketBody) {
        super(header, marketBody);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Market{");
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
