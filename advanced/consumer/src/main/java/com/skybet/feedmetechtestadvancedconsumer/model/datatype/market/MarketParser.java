package com.skybet.feedmetechtestadvancedconsumer.model.datatype.market;

import com.skybet.feedmetechtestadvancedconsumer.model.datatype.DataTypeParser;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.DataTypeParserUtils;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Header;

import org.springframework.stereotype.Component;

@Component("market")
public class MarketParser extends DataTypeParser {

    public MarketParser(final DataTypeParserUtils dataTypeParserUtils) {
        super(dataTypeParserUtils);
    }

    public Market parse(final String[] headerValues, final String bodyValue) {
        final Header header = parse(headerValues);
        final String[] marketValues = dataTypeParserUtils.splitBodyValue(bodyValue);
        final String eventId = marketValues[0];
        final String marketId = marketValues[1];
        final String name = marketValues[2];
        final boolean displayed = dataTypeParserUtils.booleanValueOf(marketValues[3]);
        final boolean suspended = dataTypeParserUtils.booleanValueOf(marketValues[4]);
        final MarketBody marketBody = new MarketBody(eventId, marketId, name, displayed, suspended);
        return new Market(header, marketBody);
    }
}

