package com.skybet.feedmetechtestadvancedconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedMeTechTestAdvancedConsumerBoot {

    public static void main(String[] args) {
        SpringApplication.run(FeedMeTechTestAdvancedConsumerBoot.class, args);
    }
}
