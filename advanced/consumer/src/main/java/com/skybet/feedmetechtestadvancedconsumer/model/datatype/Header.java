package com.skybet.feedmetechtestadvancedconsumer.model.datatype;

public class Header {

    private int msgId;
    private String operation;
    private String type;
    private long timestamp;

    protected Header() {

    }

    public Header(final int msgId, final String operation, final String type, final long timestamp) {
        this.msgId = msgId;
        this.operation = operation;
        this.type = type;
        this.timestamp = timestamp;
    }

    public int getMsgId() {
        return msgId;
    }

    public String getOperation() {
        return operation;
    }

    public String getType() {
        return type;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
