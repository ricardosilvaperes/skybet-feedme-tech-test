package com.skybet.feedmetechtestadvancedconsumer.model.datatype.event;

import com.skybet.feedmetechtestadvancedconsumer.model.datatype.DataTypeParser;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.DataTypeParserUtils;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Header;

import org.springframework.stereotype.Component;

@Component("event")
public class EventParser extends DataTypeParser {

    public EventParser(final DataTypeParserUtils dataTypeParserUtils) {
        super(dataTypeParserUtils);
    }

    public Event parse(final String[] headerValues, final String bodyValue) {
        final Header header = parse(headerValues);
        final String[] eventValues = dataTypeParserUtils.splitBodyValue(bodyValue);
        final String eventId = eventValues[0];
        final String category = eventValues[1];
        final String subCategory = eventValues[2];
        final String name = eventValues[3];
        final long startTime = Long.parseLong(eventValues[4]);
        final boolean displayed = dataTypeParserUtils.booleanValueOf(eventValues[5]);
        final boolean suspended = dataTypeParserUtils.booleanValueOf(eventValues[6]);
        final EventBody eventBody = new EventBody(eventId, category, subCategory, name, startTime, displayed, suspended);
        return new Event(header, eventBody);
    }
}

