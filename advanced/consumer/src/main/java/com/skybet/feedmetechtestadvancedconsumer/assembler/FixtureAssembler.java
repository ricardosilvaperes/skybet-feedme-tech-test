package com.skybet.feedmetechtestadvancedconsumer.assembler;

import com.skybet.feedmetechtestadvancedconsumer.domain.Fixture;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.event.Event;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.event.EventBody;

import org.springframework.stereotype.Component;

@Component
public class FixtureAssembler {

    public Fixture mapFrom(final Event event) {
        final EventBody eventBody = event.getBody();
        return new Fixture(eventBody.getEventId(), eventBody.getCategory(), eventBody.getSubCategory(), eventBody.getName(), eventBody.getStartTime(), eventBody
                .isDisplayed(), eventBody.isSuspended());
    }

    public void update(final Fixture fixture, final Event event) {
        final EventBody eventBody = event.getBody();
        fixture.setDisplayed(eventBody.isDisplayed());
        fixture.setSuspended(eventBody.isSuspended());
    }
}
