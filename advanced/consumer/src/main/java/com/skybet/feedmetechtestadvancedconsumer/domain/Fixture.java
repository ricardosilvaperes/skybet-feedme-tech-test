package com.skybet.feedmetechtestadvancedconsumer.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@Document(collection = "fixture")
public class Fixture {

    @Id private final String id;
    private final String category;
    private final String subCategory;
    private final String name;
    private final long startTime;
    private final Set<Opportunity> opportunities = new HashSet<>();
    private boolean displayed;
    private boolean suspended;

    @Version private Long version;

    public Fixture(String id,
            String category,
            String subCategory,
            String name,
            long startTime,
            boolean displayed,
            boolean suspended)
    {
        this.id = id;
        this.category = category;
        this.subCategory = subCategory;
        this.name = name;
        this.startTime = startTime;
        this.displayed = displayed;
        this.suspended = suspended;
    }

    public String getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public String getName() {
        return name;
    }

    public long getStartTime() {
        return startTime;
    }

    public Opportunity getOpportunity(final String opportunityId) {
        return opportunities.stream()
                .filter(opportunity -> opportunity.getId().equals(opportunityId))
                .findFirst()
                .orElse(null);
    }

    public Set<Opportunity> getOpportunities() {
        return opportunities;
    }

    public void addOpportunity(final Opportunity opportunity) {
        opportunities.add(opportunity);
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(final boolean displayed) {
        this.displayed = displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(final boolean suspended) {
        this.suspended = suspended;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Fixture{");
        stringBuilder.append("id='").append(id).append('\'');
        stringBuilder.append(", category='").append(category).append('\'');
        stringBuilder.append(", subCategory='").append(subCategory).append('\'');
        stringBuilder.append(", name='").append(name).append('\'');
        stringBuilder.append(", startTime=").append(startTime);
        stringBuilder.append(", displayed=").append(displayed);
        stringBuilder.append(", suspended=").append(suspended);
        stringBuilder.append(", opportunities=").append(opportunities);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
