package com.skybet.feedmetechtestadvancedconsumer.model.datatype.outcome;

import com.skybet.feedmetechtestadvancedconsumer.model.datatype.DataType;
import com.skybet.feedmetechtestadvancedconsumer.model.datatype.Header;

public class Outcome extends DataType<OutcomeBody> {

    protected Outcome() {

    }

    public Outcome(final Header header, final OutcomeBody outcomeBody) {
        super(header, outcomeBody);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Outcome{");
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
