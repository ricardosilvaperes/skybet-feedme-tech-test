package com.skybet.feedmetechtestbasic.model.datatype.market;

import com.skybet.feedmetechtestbasic.model.datatype.DataTypeBodyValidator;

import org.springframework.stereotype.Component;

@Component
public class MarketBodyValidator extends DataTypeBodyValidator {

    private static final String MARKET_PATTERN_REGEX = UUID_REGEX + "\\|" + UUID_REGEX + "\\|.+\\|(0|1)\\|(0|1)";

    public MarketBodyValidator() {
        super(MARKET_PATTERN_REGEX);
    }
}
