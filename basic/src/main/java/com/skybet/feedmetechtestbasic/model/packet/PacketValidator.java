package com.skybet.feedmetechtestbasic.model.packet;

import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class PacketValidator {

    private static final Pattern PACKET_PATTERN = Pattern.compile("\\|\\d+\\|(create|update)\\|(event|market|outcome)\\|\\d+\\|.*\\|");

    public boolean dataIsValid(final String data) {
        return PACKET_PATTERN.matcher(data).matches();
    }
}
