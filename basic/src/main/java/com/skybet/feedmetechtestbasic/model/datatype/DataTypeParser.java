package com.skybet.feedmetechtestbasic.model.datatype;

public abstract class DataTypeParser {

    protected DataTypeParserUtils dataTypeParserUtils;
    protected DataTypeBodyValidator dataTypeBodyValidator;

    protected DataTypeParser(final DataTypeParserUtils dataTypeParserUtils, final DataTypeBodyValidator dataTypeBodyValidator) {
        this.dataTypeParserUtils = dataTypeParserUtils;
        this.dataTypeBodyValidator = dataTypeBodyValidator;
    }

    protected static Header parse(final String[] headerValues) {
        final int msgId = Integer.parseInt(headerValues[0]);
        final String operation = headerValues[1];
        final String type = headerValues[2];
        final long timestamp = Long.parseLong(headerValues[3]);
        return new Header(msgId, operation, type, timestamp);
    }

    public abstract DataType parse(final String[] headerValues, final String bodyValue);
}

