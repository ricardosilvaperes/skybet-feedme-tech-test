package com.skybet.feedmetechtestbasic.model.datatype.market;

import com.skybet.feedmetechtestbasic.model.datatype.DataType;
import com.skybet.feedmetechtestbasic.model.datatype.Header;

public final class Market extends DataType<MarketBody> {

    public Market(final Header header, final MarketBody marketBody) {
        super(header, marketBody);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Market{");
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
