package com.skybet.feedmetechtestbasic.model.exception;

public final class InvalidPacketException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "invalid packet data detected: %s";

    public InvalidPacketException(final String data) {
        super(String.format(MESSAGE_TEMPLATE, data));
    }
}
