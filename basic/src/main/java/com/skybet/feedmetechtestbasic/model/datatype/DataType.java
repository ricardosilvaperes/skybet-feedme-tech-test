package com.skybet.feedmetechtestbasic.model.datatype;

public abstract class DataType<T extends Body> {

    private final Header header;
    private final T body;

    protected DataType(final Header header, final T body) {
        this.header = header;
        this.body = body;
    }

    public Header getHeader() {
        return header;
    }

    public T getBody() {
        return body;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("header=").append(header);
        stringBuilder.append(", body=").append(body);
        return stringBuilder.toString();
    }
}
