package com.skybet.feedmetechtestbasic.model.datatype.outcome;

import com.skybet.feedmetechtestbasic.model.datatype.Body;

public final class OutcomeBody extends Body {

    private final String marketId;
    private final String outcomeId;
    private final String name;
    private final String price;

    public OutcomeBody(final String marketId, final String outcomeId, final String name, final String price, final boolean displayed, final boolean suspended) {
        super(displayed, suspended);
        this.marketId = marketId;
        this.outcomeId = outcomeId;
        this.name = name;
        this.price = price;
    }

    public String getMarketId() {
        return marketId;
    }

    public String getOutcomeId() {
        return outcomeId;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{marketId='").append(marketId).append('\'');
        stringBuilder.append(", outcomeId='").append(outcomeId).append('\'');
        stringBuilder.append(", name='").append(name).append('\'');
        stringBuilder.append(", price='").append(price).append('\'');
        stringBuilder.append(super.toString());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
