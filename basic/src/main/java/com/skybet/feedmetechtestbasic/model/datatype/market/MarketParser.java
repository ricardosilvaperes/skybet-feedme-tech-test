package com.skybet.feedmetechtestbasic.model.datatype.market;

import com.skybet.feedmetechtestbasic.model.datatype.DataTypeParser;
import com.skybet.feedmetechtestbasic.model.datatype.DataTypeParserUtils;
import com.skybet.feedmetechtestbasic.model.datatype.Header;
import com.skybet.feedmetechtestbasic.model.exception.InvalidPacketException;

import org.springframework.stereotype.Component;

@Component("market")
public class MarketParser extends DataTypeParser {

    public MarketParser(final DataTypeParserUtils dataTypeParserUtils, final MarketBodyValidator marketValidator) {
        super(dataTypeParserUtils, marketValidator);
    }

    public Market parse(final String[] headerValues, final String bodyValue) {
        if (dataTypeBodyValidator.dataIsValid(bodyValue)) {
            final Header header = parse(headerValues);
            final String[] marketValues = dataTypeParserUtils.splitBodyValue(bodyValue);
            final String eventId = marketValues[0];
            final String marketId = marketValues[1];
            final String name = marketValues[2];
            final boolean displayed = dataTypeParserUtils.booleanValueOf(marketValues[3]);
            final boolean suspended = dataTypeParserUtils.booleanValueOf(marketValues[4]);
            final MarketBody marketBody = new MarketBody(eventId, marketId, name, displayed, suspended);
            return new Market(header, marketBody);
        }
        throw new InvalidPacketException(bodyValue);
    }
}

