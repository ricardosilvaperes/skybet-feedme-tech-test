package com.skybet.feedmetechtestbasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedMeTechTestBasicBoot {

    public static void main(String[] args) {
        SpringApplication.run(FeedMeTechTestBasicBoot.class, args);
    }
}
