package com.skybet.feedmetechtestbasic.model.packet;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class PacketValidatorTest {

    private final PacketValidator packetValidator = new PacketValidator();

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidMsgId() {
        // Given
        final String data = "|invalidMsgId|create|event|1|body|";

        // When
        final boolean result = packetValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidOperation() {
        // Given
        final String data = "|1|invalidOperation|event|1|body|";

        // When
        final boolean result = packetValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidTimestamp() {
        // Given
        final String data = "|1|create|event|invalidTimestamp|body|";

        // When
        final boolean result = packetValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidType() {
        // Given
        final String data = "|1|create|invalidType|1|body|";

        // When
        final boolean result = packetValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnTrue() {
        // Given
        final String data = "|1|create|event|1|body|";

        // When
        final boolean result = packetValidator.dataIsValid(data);

        // Then
        assertThat(result).isTrue();
    }
}
