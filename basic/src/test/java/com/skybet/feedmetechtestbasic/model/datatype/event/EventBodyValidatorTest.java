package com.skybet.feedmetechtestbasic.model.datatype.event;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class EventBodyValidatorTest {

    private final EventBodyValidator eventBodyValidator = new EventBodyValidator();

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidEventId() {
        // Given
        final String data = "invalidEventId|category|subCategory|name|1|0|1";

        // When
        final boolean result = eventBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidCategory() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000||subCategory|name|1|0|1";

        // When
        final boolean result = eventBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidSubCategory() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|category||name|1|0|1";

        // When
        final boolean result = eventBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidName() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|category|subCategory||1|0|1";

        // When
        final boolean result = eventBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidStartTime() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|category|subCategory|name|invalidStartTime|0|1";

        // When
        final boolean result = eventBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidDisplayedValue() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|category|subCategory|name|1|invalidDisplayedValue|1";

        // When
        final boolean result = eventBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnFalseDueToInvalidSuspendedValue() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|category|subCategory|name|1|0|invalidSuspendedValue";

        // When
        final boolean result = eventBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    public void dataIsValidShouldReturnTrue() {
        // Given
        final String data = "00000000-0000-0000-0000-000000000000|category|subCategory|name|1|0|1";

        // When
        final boolean result = eventBodyValidator.dataIsValid(data);

        // Then
        assertThat(result).isTrue();
    }
}
